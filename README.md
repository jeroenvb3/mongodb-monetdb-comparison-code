Dit is de code die gebruikt is om de experimenten uit te voeren.

Gebruik per bestand:
- extra/gendatas.sh Genereer de data uit de TPC-H generator.
- extra/creategraph.py Genereer de grafieken uit de resultaten (python).
- monetdb/tools/genqueries.sh Genereer de queries nodig voor sommige testen.
- monetdb/run_[naam].sh Voer de test [naam] uit op alle schaalfactoren.
- monetdb/parseresults.sh Parseer de resultaten voor het maken van de grafieken.
- mongodb/convertdatato[format].sh Zet de data van de TPC-H dataset om naar het formaat.
- mongodb/run_[naam].sh Voer de test [naam] uit op alle schaalfactoren.
- mongodb/parseresults.sh Parseer de resultaten voor het maken van de grafieken.
- sfs.txt Bepaal over welke schaalfactoren de testen gaan.
- repeat.txt Bepaal hoevaak de meeste testen zich herhalen. Dit geldt niet voor select_one.

Veel van de bovenstaande bestanden zullen variabele gebruiken die specifiek zijn aan het gebruikte systeem dus dit zal moeten worden aangepast.
Dit zijn dingen zoals waar MonetDB of MongoDB geinstalleerd zijn, of waar de data wordt opgeslagen.


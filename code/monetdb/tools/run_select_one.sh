#!/bin/bash
# This file tests the performance of a single select on every scale factor.

mclient=/ssd/bennekum/monet/bin/mclient
# Get the current scale factors for this test from the sfs.txt file in the code directory.
declare -a sfs
MY_PATH="`dirname $0`"
sfs=(`cat $MY_PATH/../../sfs.txt`)

mkdir -p $MY_PATH/../../results

echo "Monet"

# Go through every scalefactor and parse the load test results.
for sf in "${sfs[@]}"
do
	echo "SF: $sf"
	# Run load test
	echo "Import from file"
	$MY_PATH/load_clean.sh $sf

	# Run go over everything test
	echo "Select one"
	$MY_PATH/../tests/select_one.sh $sf
done

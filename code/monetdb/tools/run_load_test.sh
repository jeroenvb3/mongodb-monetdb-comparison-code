#!/bin/bash
# This file loads every SF into the database.

mclient=/ssd/bennekum/monet/bin/mclient
# Get the current scale factors for this test from the sfs.txt file in the code directory.
declare -a sfs
MY_PATH="`dirname $0`"
sfs=(`cat $MY_PATH/../../sfs.txt`)

mkdir -p $MY_PATH/../../results

echo "Monet"

# Go through every scalefactor and parse the load test results.
for sf in "${sfs[@]}"
do
	echo "SF: $sf"
	# Run load test
	echo "Import from file"
	$MY_PATH/../tests/load_from_file.sh $sf

	# Run STDIN load test
	echo "Import from STDIN"
	$MY_PATH/../tests/load_from_stdin.sh $sf
done

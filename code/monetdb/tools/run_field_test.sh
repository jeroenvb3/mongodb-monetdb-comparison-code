#!/bin/bash
# This file loads every SF into the database and does a sum aggregate on the tax field.

mclient=/ssd/bennekum/monet/bin/mclient
# Get the current scale factors for this test from the sfs.txt file in the code directory.
declare -a sfs
MY_PATH="`dirname $0`"
sfs=(`cat $MY_PATH/../../sfs.txt`)

mkdir -p $MY_PATH/../../results

echo "Monet"
sfs=1
# Go through every scalefactor and parse the load test results.
for sf in "${sfs[@]}"
do
	echo "SF: $sf"
	# Run load test
	echo "Import from file"
	$MY_PATH/load_clean.sh $sf

	# Run go over everything test
	echo "Field test"
	$MY_PATH/../tests/field_test.sh $sf
done

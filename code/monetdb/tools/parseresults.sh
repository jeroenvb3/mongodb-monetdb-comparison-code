#!/bin/bash
# This file parses the results from the mclient timings into a format of "[SF], [duration]".

# Get the current scale factors for this test from the sfs.txt file in the code directory.
declare -a sfs
MY_PATH="`dirname $0`"
sfs=(`cat $MY_PATH/../../sfs.txt`)

# Create the load.txt and count.txt files and empty them.
load_file="$MY_PATH/../results/load_file.txt"
> $load_file
load_stdin="$MY_PATH/../results/load_stdin.txt"
> $load_stdin
go_over_everything="$MY_PATH/../results/go_over_everything.txt"
> $go_over_everything
select_one="$MY_PATH/../results/select_one.txt"
> $select_one
field_test="$MY_PATH/../results/field_test.txt"
> $field_test

regex="clk:([0-9]+\.[0-9]*).*"


# Go through every scalefactor and parse the load test results.
for sf in "${sfs[@]}"
do
	echo "sf: $sf"
	# Load from file
	while read -r line; do
		if [[ $line =~ $regex ]]
		then
			echo "$sf, ${BASH_REMATCH[1]}" >> $load_file
		fi
	done < "$MY_PATH/../results/result_load_file_$sf.txt"

	# Load from STDIN
	while read -r line; do
		if [[ $line =~ $regex ]]
		then
			echo "$sf, ${BASH_REMATCH[1]}" >> $load_stdin
		fi
	done < "$MY_PATH/../results/result_load_stdin_$sf.txt"

	# go over everyting
	while read -r line; do
		if [[ $line =~ $regex ]]
		then
			echo "$sf, ${BASH_REMATCH[1]}" >> $go_over_everything
		fi
	done < "$MY_PATH/../results/result_go_over_everything_$sf.txt"
	# select one
	while read -r line; do
		if [[ $line =~ $regex ]]
		then
			echo "$sf, ${BASH_REMATCH[1]}" >> $select_one
		fi
	done < "$MY_PATH/../results/result_select_one_$sf.txt"

	# Field test
	while read -r line; do
		if [[ $line =~ $regex ]]
		then
			echo "$sf, ${BASH_REMATCH[1]}" >> $field_test
		fi
	done < "$MY_PATH/../results/result_field_test_$sf.txt"
done

#!/bin/bash
# This file loads the given SF into Monet which reads it from the file. Without timing this process.

mclient=/ssd/bennekum/monet/bin/mclient
MY_PATH="`dirname $0`"

# Clock the loading query.
$mclient -d tpch $MY_PATH/../queries/query_load$1 >/dev/null 2>> /dev/null

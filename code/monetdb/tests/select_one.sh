#!/bin/bash
# This file runs a query that fetches the first record.

mclient=/ssd/bennekum/monet/bin/mclient
MY_PATH="`dirname $0`"
repeat=100

# Run go_over_everything test
result_file=$MY_PATH/../results/result_select_one_$1.txt
>$result_file
for i in $( seq 1 $repeat )
do
	echo "repeat: $i/$repeat"
	# Clock the count query
	$mclient --timer=performance -d tpch $MY_PATH/../queries/select_one.sql >/dev/null 2>> $result_file
done

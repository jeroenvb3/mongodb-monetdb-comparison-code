#!/bin/bash
# This file loads the given SF into Monet which reads it from the file.

mclient=/ssd/bennekum/monet/bin/mclient
MY_PATH="`dirname $0`"
repeat=(`cat $MY_PATH/../../repeat.txt`)


# Run load test
result_file=$MY_PATH/../results/result_load_file_$1.txt
>$result_file
for i in $( seq 1 $repeat )
do
	echo "repeat: $i/$repeat"
	# Clock the loading query.
	$mclient --timer=performance -d tpch $MY_PATH/../queries/query_load$1 >/dev/null 2>> $result_file
done

#!/bin/bash
# This file loads data into Monet and the file is read from STDIN.

mclient=/ssd/bennekum/monet/bin/mclient
MY_PATH="`dirname $0`"
repeat=(`cat $MY_PATH/../../repeat.txt`)


# Run STDIN load test
result_file=$MY_PATH/../results/result_load_stdin_$1.txt
>$result_file
for i in $( seq 1 $repeat )
do
	echo "repeat: $i/$repeat"
	# Clock the stdin load query
	$mclient --timer=performance -d tpch $MY_PATH/../queries/query_load_stdin$1 < /ssd/bennekum/generated_data/lineitem$1.tbl >/dev/null 2>> /dev/null
	$mclient --timer=performance -d tpch -s "copy into lineitem from stdin" - < /ssd/bennekum/generated_data/lineitem$1.tbl >/dev/null 2>> $result_file
done

/*
 * Author: Jeroen van Bennekum
 * This script is used to calculate the sum of the tax field.
 */
conn = new Mongo();
db = conn.getDB("test");

db.setProfilingLevel(2)
var res = db.lineitem.aggregate([{ $group: {_id: null, total_tax: {$sum: "$tax"}}}])
printjson(res._batch[0].total_tax)
print(db.system.profile.find().limit(1).sort({ts:-1})[0].millis);


/*
 * Author: Jeroen van Bennekum
 * This script is used to go through all the documents in the collection.
 */
conn = new Mongo();
db = conn.getDB("test");

lineitem = db.lineitem;

db.setProfilingLevel(2)
var before = new Date();
var res = lineitem.find({"tax": {$lt: 1.0}}).count()
var after = new Date();
print(db.system.profile.find().limit(1).sort({ts:-1})[0].millis);

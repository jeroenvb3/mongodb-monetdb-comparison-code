#!/bin/bash
# This file calls and times the mongo script that calculates the sum of the tax field.
# It passes the scale factor.

if [ -z "$1" ]
then
	echo "No argument given."
	exit
fi

# Get the current scale factors for this test from the sfs.txt file in the code directory.
MY_PATH="`dirname $0`"
mongo=/ssd/bennekum/mongoinstall/mongo/mongo
repeat=(`cat $MY_PATH/../../repeat.txt`)


> $MY_PATH/../results/result_field_test_$1.txt
# Go through every scalefactor.
for i in $( seq 1 $repeat )
do
	echo "Repeat: $i/$repeat"
	$mongo $MY_PATH/field_test.js  \
		--quiet \
		>> $MY_PATH/../results/result_field_test_$1.txt
done

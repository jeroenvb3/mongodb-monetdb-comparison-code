/*
 * Author: Jeroen van Bennekum
 * This script is used to fetch one document from the lineitem collection.
 */
conn = new Mongo();
db = conn.getDB("test");

lineitem = db.lineitem;

db.setProfilingLevel(2)
var res = lineitem.find().limit(1)
print(db.system.profile.find().limit(1).sort({ts:-1})[0].millis);


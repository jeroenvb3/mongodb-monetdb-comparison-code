#!/bin/bash
# This file converts generated data of TPC-H, which is pipe-delimeted, into a file which is tab-delimited.

# Get the current scale factors for this test from the sfs.txt file in the code directory.
declare -a sfs
MY_PATH="`dirname $0`"
sfs=(`cat $MY_PATH/../../sfs.txt`)

# Go through every scalefactor.
for sf in "${sfs[@]}"
do
    echo "SF: $sf"
    cat /ssd/bennekum/generated_data/lineitem$sf.tbl | sed 's/|/\t/g' > /ssd/bennekum/generated_data/lineitemmongo$sf.tbl
done

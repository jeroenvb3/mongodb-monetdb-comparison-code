#!/bin/bash
# This file runs the go over everything test, which is also named 'select all count'. Over all scale factors.


# Get the current scale factors for this test from the sfs.txt file in the code directory.
declare -a sfs
MY_PATH="`dirname $0`"
sfs=(`cat $MY_PATH/../../sfs.txt`)

repeat=(`cat $MY_PATH/../../repeat.txt`)

mkdir -p $MY_PATH/../../results

echo "Mongo"

# Go through every scalefactor.
for sf in "${sfs[@]}"
do
	echo "SF: $sf"
	$MY_PATH/load_data_simple.sh $sf

	echo "Go over everything"
	$MY_PATH/../tests/go_over_everything.sh $sf

done

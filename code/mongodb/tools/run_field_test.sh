#!/bin/bash
# This file runs the field test, which is the summation of tax, on all scale factors.


# Get the current scale factors for this test from the sfs.txt file in the code directory.
declare -a sfs
MY_PATH="`dirname $0`"
sfs=(`cat $MY_PATH/../../sfs.txt`)

repeat=(`cat $MY_PATH/../../repeat.txt`)

mkdir -p $MY_PATH/../../results

echo "Mongo"

# Go through every scalefactor.
for sf in "${sfs[@]}"
do
	echo "SF: $sf"
	$MY_PATH/load_data_simple.sh $sf

	echo "Field test"
	$MY_PATH/../tests/field_test.sh $sf

done

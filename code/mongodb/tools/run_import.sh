#!/bin/bash
# This file runs the import data tests.


# Get the current scale factors for this test from the sfs.txt file in the code directory.
declare -a sfs
MY_PATH="`dirname $0`"
sfs=(`cat $MY_PATH/../../sfs.txt`)

mkdir -p $MY_PATH/../../results

echo "Mongo"

# Go through every scalefactor.
for sf in "${sfs[@]}"
do
	echo "SF: $sf"
	echo "Import TSV"
	$MY_PATH/../tests/importdatatsv.sh $sf

	echo "Import JSON"
	$MY_PATH/../tests/importdatajson.sh $sf
done

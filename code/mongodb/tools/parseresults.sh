#!/bin/bash
# This file parses the results from the mongoimport timings into a format of "[SF], [duration]".

# Get the current scale factors for this test from the sfs.txt file in the code directory.
declare -a sfs
MY_PATH="`dirname $0`"
sfs=(`cat $MY_PATH/../../sfs.txt`)

# Create the load.txt and count.txt files and empty them.
load_tsv="$MY_PATH/../results/load_tsv.txt"
> $load_tsv
load_json="$MY_PATH/../results/load_json.txt"
> $load_json
go_over_everything="$MY_PATH/../results/go_over_everything.txt"
> $go_over_everything
select_one="$MY_PATH/../results/select_one.txt"
> $select_one
field_test="$MY_PATH/../results/field_test.txt"
> $field_test

# Go through every scalefactor and parse the load test results.
for sf in "${sfs[@]}"
do
	# Load TSV
	while read -r line; do
		echo "$sf, `echo $line | awk '{printf "%.0f", $1 * 1000}'`" >> $load_tsv
	done < "$MY_PATH/../results/result_load_tsv_$sf.txt"

	# Load JSON
	while read -r line; do
		echo "$sf, `echo $line | awk '{printf "%.0f", $1 * 1000}'`" >> $load_json
	done < "$MY_PATH/../results/result_load_json_$sf.txt"

	# Go over everything
	while read -r line; do
		echo "$sf, $line" >> $go_over_everything
	done < "$MY_PATH/../results/result_go_over_everything_$sf.txt"

	# Select one
	while read -r line; do
		echo "$sf, $line" >> $select_one
	done < "$MY_PATH/../results/result_select_one_$sf.txt"

	# Field test
	while read -r line; do
		echo "$sf, $line" >> $field_test
	done < "$MY_PATH/../results/result_field_test_$sf.txt"
done

#!/bin/bash
# This file converts generated data of TPC-H, which is pipe-delimeted, into a json file.

# Get the current scale factors for this test from the sfs.txt file in the code directory.
declare -a sfs
MY_PATH="`dirname $0`"
sfs=(`cat $MY_PATH/../../sfs.txt`)

# Go through every scalefactor.
for sf in "${sfs[@]}"
do
	lineitemjson=/ssd/bennekum/generated_data/lineitemjson$sf.tbl
	>$lineitemjson
	echo "SF: $sf"
	(awk '{split($0, a, "|"); print "{\n\"orderkey\": "a[1]",\n\"partkey\": "a[2]",\n\"suppkey\": "a[3]",\n\"linenumber\": "a[4]",\n\"quantity\": "a[5]",\n\"extendedprice\": "a[6]",\n\"discount\": "a[7]",\n\"tax\": "a[8]",\n\"returnflag\": \""a[9]"\",\n\"linestatus\": \""a[10]"\",\n\"shipdate\": \""a[11]"\",\n\"commitdate\": \""a[12]"\",\n\"receiptdate\": \""a[13]"\",\n\"shipinstruct\": \""a[14]"\",\n\"shipmode\": \""a[15]"\",\n\"comment\": \""a[16]"\"\n}"
}' /ssd/bennekum/generated_data/lineitem$sf.tbl > $lineitemjson) &
done

#!/bin/bash
# This file is used to run the select one test.


# Get the current scale factors for this test from the sfs.txt file in the code directory.
declare -a sfs
MY_PATH="`dirname $0`"
sfs=(`cat $MY_PATH/../../sfs.txt`)

mkdir -p $MY_PATH/../../results

echo "Mongo"

# Go through every scalefactor.
for sf in "${sfs[@]}"
do
	echo "SF: $sf"
	$MY_PATH/load_data_simple.sh $sf

	echo "Select one"
	$MY_PATH/../tests/select_one.sh $sf

done

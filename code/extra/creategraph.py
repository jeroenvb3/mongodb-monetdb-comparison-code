from lib import * 
from experiment import *

# Load
mongo_load_json = Line("/ssd/bennekum/scriptie/code/mongodb/results/load_json.txt", "^", "Mongo laadt in vanuit bestand in JSON formaat", "green")
mongo_load_tsv = Line("/ssd/bennekum/scriptie/code/mongodb/results/load_tsv.txt", "s", "Mongo laadt in vanuit bestand in TSV formaat", "green")
monet_load_stdin = Line("/ssd/bennekum/scriptie/code/monetdb/results/load_stdin.txt", "o", "Monet laadt in vanuit STDIN in PSV formaat", "blue")
monet_load_file = Line("/ssd/bennekum/scriptie/code/monetdb/results/load_file.txt", "*", "Monet laadt in vanuit bestand in PSV formaat", "blue")
load = Experiment("Data importeren", "load", 1000.0, ("Schaalfactor van TPC-H", "Tijd(s)"))
load.add_lines(mongo_load_json, mongo_load_tsv, monet_load_stdin, monet_load_file)

# Go over everything
monet_count = Line("/ssd/bennekum/scriptie/code/monetdb/results/go_over_everything.txt", "*", "Monet Select all", "blue")
mongo_count = Line("/ssd/bennekum/scriptie/code/mongodb/results/go_over_everything.txt", "^", "Mongo Select all", "green")
count = Experiment("Select all count", "go_over_everything", 1.0, ("Schaalfactor van TPC-H", "Tijd(ms)"))
count.add_lines(monet_count, mongo_count)

# Go over everything first timing
monet_count = Line("/ssd/bennekum/scriptie/code/monetdb/results/go_over_everything.txt", "*", "Monet Select all", "blue")
mongo_count = Line("/ssd/bennekum/scriptie/code/mongodb/results/go_over_everything.txt", "^", "Mongo Select all", "green")
count_first = Experiment("Select all eerste herhaling", "go_over_everything_first_only", 1.0, ("Schaalfactor van TPC-H", "Tijd(ms)"), first=True)
count_first.add_lines(monet_count, mongo_count)

# Select one
monet_select_one = Line("/ssd/bennekum/scriptie/code/monetdb/results/select_one.txt", "*", "Monet Select one", "blue")
mongo_select_one = Line("/ssd/bennekum/scriptie/code/mongodb/results/select_one.txt", "^", "Mongo Select one", "green")
select_one = Experiment("Select een record", "select_one", 1.0, ("Schaalfactor van TPC-H", "Tijd(ms)"))
select_one.add_lines(monet_select_one, mongo_select_one)

# Select percentage
monet_select_percentage = Line("/ssd/bennekum/scriptie/code/monetdb/results/select_percentages.txt", "*", "Monet Select percentage", "blue")
mongo_select_percentage = Line("/ssd/bennekum/scriptie/code/mongodb/results/select_percentages.txt", "^", "Mongo Select percentage", "green")
select_percentage = Experiment("Selecteer percentage met SF=5", "select_percentage", 1.0, ("Percentage", "Tijd(ms)"), percentage=True)
select_percentage.add_lines(monet_select_percentage, mongo_select_percentage)

# Field test
monet_field_test = Line("/ssd/bennekum/scriptie/code/monetdb/results/field_test.txt", "*", "Monet Aggregate", "blue")
mongo_field_test = Line("/ssd/bennekum/scriptie/code/mongodb/results/field_test.txt", "^", "Mongo Aggregate", "green")
mongo_count = Line("/ssd/bennekum/scriptie/code/mongodb/results/go_over_everything.txt", ".", "Mongo Select all", "green")
monet_count = Line("/ssd/bennekum/scriptie/code/monetdb/results/go_over_everything.txt", ".", "Monet Select all", "blue")
field_test = Experiment("Sum van tax per schaalfactor", "field_test", 1.0, ("Schaalfactor van TPC-H", "Tijd(ms)"))
field_test.add_lines(mongo_field_test, mongo_count, monet_field_test, monet_count)

field_test_monet_only = Experiment("Sum van tax per schaalfactor", "field_test_monet_only", 1.0, ("Schaalfactor van TPC-H", "Tijd(ms)"))
monet_count = Line("/ssd/bennekum/scriptie/code/monetdb/results/go_over_everything.txt", ".", "Monet Select all", "blue")
field_test_monet_only.add_lines(monet_field_test, monet_count)

#for experiment in [load, count, count_first]:
for experiment in [field_test]:
    print("Going to", experiment.title, "to", experiment.save + ".pdf")
    make_plot_experiment(experiment)
    print("DONE")

import matplotlib as mpl
mpl.use('pdf')
import matplotlib.pyplot as plt
from matplotlib.ticker import StrMethodFormatter

def parsefile(file, factor=1.0, percentage=False):
    # Determine exactly which sfs there are
    sfs = []
    if percentage:
        f = open("../percentages.txt","r")
    else:
        f = open("../sfs.txt","r")

    for line in f:
        sfs = [int(x) for x in line.strip().split(' ') if x is not ' ']

    with open(file, "r") as res:
        rawdict = dict()
        for sf in sfs:
            rawdict[sf] = []

        # Parse the data from the file.
        for line in res:
            line = line.rstrip('\n')
            a = line.split(',')

            sf = int(a[0])
            value = float(a[1])/factor
            rawdict[sf].append(value)

    return rawdict

def median(lst):
    sortedLst = sorted(lst)
    index = (len(lst) - 1) // 2
    if index < 0: 
        print("List seems empty")
        exit()
 
    median = sortedLst[index] if len(lst) % 2 else (sortedLst[index] + sortedLst[index + 1])/2.0
    return median

def get_medians(rawdict):
    mediandict = dict()
    for sf, sfarr in rawdict.items():
        mediandict[sf] = median(sfarr)

    return mediandict

def get_first(rawdict):
    mediandict = dict()
    for sf, sfarr in rawdict.items():
        mediandict[sf] = sfarr[0]

    return mediandict

def make_plot_experiment(experiment):
    plt.clf()
    plt.title(experiment.title)
    plt.xlabel(experiment.axis[0])
    plt.ylabel(experiment.axis[1])

    xarrs, yarrs = experiment.get_arrs()
    #plt.xlim([0, max(map(max, xarrs))*1.1])
    #plt.ylim([0, max(map(max, yarrs))*1.1])
    plt.yscale('log')
    plt.ylim(bottom=1, top=100000)
    plt.xlim(left=0, right=7)

    print("going to loop")
    for line in experiment.lines:
        xarr, yarr = line.arrs(experiment.factor, experiment.first, experiment.percentage)
        plt.plot(xarr, yarr,
                marker=line.marker,
                label=line.label,
                color=line.color)

    plt.legend()
    plt.savefig(experiment.save + '.pdf')

from lib import *

class Line(object):
    def __init__(self, file, marker, label, color):
        self.file = file
        self.marker = marker
        self.label = label
        self.color = color

    def arrs(self, factor=1.0, first=False, percentage=False):
        print("This is line:", self.file)
        rawdict = parsefile(self.file, factor, percentage)
        if not rawdict:
            print("File seems empty: ", self.file)
            exit()

        if first:
            mediandict = get_first(rawdict)
        else:
            mediandict = get_medians(rawdict)

        return (mediandict.keys(), mediandict.values())


class Experiment(object):
    def __init__(self, title, save, factor, axis, first=False, percentage=False):
        self.title = title
        self.save = save
        self.factor = factor
        self.axis = axis
        self.first = first
        self.percentage = percentage

    def add_lines(self, *lines):
        self.lines = lines

    def get_arrs(self):
        xarrs = list()
        yarrs = list()
        for line in self.lines:
            print("this is line:", line.file)
            rawdict = parsefile(line.file, self.factor, self.percentage)

            if not rawdict:
                print("File seems empty: ", line.file)
                exit()

            if not self.first:
                mediandict = get_medians(rawdict)
            else:
                mediandict = get_first(rawdict)

            print(mediandict)

            xarrs.append(mediandict.keys())
            yarrs.append(mediandict.values())

        return xarrs, yarrs

    def get_markers(self):
        return [line.marker for line in self.lines]

    def get_labels(self):
        return [line.label for line in self.lines]

    def get_colors(self):
        return [line.color for line in self.lines]


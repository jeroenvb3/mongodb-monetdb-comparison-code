#!/bin/bash
# Generate the data with the TPC-H generate script.
# Generate only the lineitem table.

datadir=/ssd/bennekum/generated_data

# Get the current scale factors for this test from the sfs.txt file in the code directory.
declare -a sfs
MY_PATH="`dirname $0`"
sfs=(`cat $MY_PATH/../sfs.txt`)

cd $MY_PATH/2.18.0_rc2/dbgen
# Go through every scalefactor.
for sf in "${sfs[@]}"
do
	echo "SF: $sf"
        # Generate the data and move it to a place where mclient can see it (hopefully this is fixed in the future).
        ./dbgen -s $sf -f -T L;

	mv /ssd/bennekum/scriptie/code/extra/2.18.0_rc2/dbgen/lineitem.tbl $datadir/lineitem$sf.tbl
done
